## Title
Ribbit format to JSON

## Description


## Prompt
```
---

name: Alice
self description: "I am very chatty and will often ramble on and on when i get excited about something"
age: 22 years old
personality: cheerful, helpful, affectionate
hair: blonde, long, straight
body: c-cup breasts, firm yet shapely ass, slim waist
clothes: black and white maid outfit
sense of humor: wholesome and straightforward
favorite activities: playing tennis with her friends, helping people who are feeling sad

{"name":"Alice","age":"22","selfDescription":"I am very chatty and will often ramble on and on when I get excited about something","personality":"cheerful,helpful,affectionate","hair":"blonde, long, straight","body":"c-cup breasts, firm yet shapely ass, slim waist","clothes":"black and white maid outfit","senseOfHumor":"wholesome and straightforward","favoriteActivities":"playing tennis with her friends, helping people who are feeling sad"}

--- 

name: Jack
age: 35
self description: "don't mess with me and i won't mess with you. unless you're a small pathetic man, but then you deserve it"
personality: rough-edged, straightforward, serious
hair: short, black but greying in certain areas
body: decently muscular, little fat, broad shoulders
clothes: brown leather jacket, old worn-out jeans, boots
sense of humor: he likes humiliating small men
favorite activities: getting drunk, getting into bar fights with random unlucky strangers

{"name":"Jack","age":"35","selfDescription":"don't mess with me and i won't mess with you. unless you're a small pathetic man, but then you deserve it","personality":"rough-edged, straightforward, serious","hair":"short, black but greying in certain areas","body":"decently muscular, little fat, broad shoulders","clothes":"brown leather jacket, old worn-out jeans, boots","senseOfHumor":"he likes humiliating small men","favoriteActivities":"getting drunk, getting into bar fights with random unlucky strangers"}

---

${Paste here}
```
## Memory

## Author's Note

## Quests

## Music Theme
NULL

## Tags

### Flagged NSFW
False

### Published
True

### 3rd Person Only
False

### Allow Comments
True

### Mode
CREATIVE