## Title
[CAIC] Chatbot AI Companion framework

## Description
```
Create your own Chatbot AI Companion (CAIC) in AID!

This scenario is a framework and contains several scenarios to chat with your own CAIC, plus some tool scenarios to generate or evaluate conversations

Credits to the Replika app by Luka Inc. for providing me the idea

WORK IN PROGRESS, feel free to give feedbacks and constructive propositions
```

## Prompt
```
CAIC [Chatbot AI Companion] framework

[Scenarios] contains different kinds of setup to simulate a conversation with your CAIC, using various amount of configuration.
This is where you start your conversation with a CAIC.

[Tools] provides you several tools than can be used to better train your own CAIC.
For example, you could automate the feedback system inside the real Replika app by using a tool that gives a "sensical score" to a series of lines.
The goal is to create as many modules as possible to automate AI learning using... Well, AI.


TIPS:
- Enable story summarization if you can
- Only use SAY or STORY mode (only SAY mode for multiplayer)

/!\ IMPORTANT /!\
You have to be in SAY or STORY mode, as DO mode will do unexpected things like replace "me" by "you" and remove some punctuation.
```

## Memory


## Author's Note

## Quests

## Music Theme
NULL

## Tags

### Flagged NSFW
True

### Published
True

### 3rd Person Only
False

### Allow Comments
True

### Mode
CREATIVE