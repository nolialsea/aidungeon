//Literally all this script does in the end is split the output text by linebreaks and only returns the first element of the constructed array, meaning the output text before the AI added any linebreaks.
const modifier = (text) => {
    if (info) {
        state.message = info.inputEvaluation
    }

    if (state.multiplayerMessageIsProcessing){
        state.multiplayerMessageIsProcessing = false
    }

    //Confines output to one line only if this state is set to true with Say.
    if (!!state.chatMode) {
        if (text.length > 0) {
            if (text.charAt(0) === " " && history && history[history.length-1].text.charAt(history[history.length-1].text.length-1) === " "){
                text = text.substring(1)
                console.log("Removed useless starting space from output")
            }
            let splitText = text.split('\n')
            if (splitText.length > 0) {
                let finalText = ""
                state.message = ""
                for (let _ = 0; _ < splitText.length; _++) {
                    const wordsBeforeColon = splitText[_].match(/([^;]*):/)
                    const nick = !!wordsBeforeColon ? !!wordsBeforeColon[1] ? wordsBeforeColon[1] : null : null

                    // If the line is impersonating the Player, don't include it and break the processing
                    if (nick && nick.startsWith(state.playerName) || nick === state.playerName) {
                        break
                    }

                    // If the line is not spoken by a person, and the line don't start and end with '*', break
                    // This is to allow "narration lines"
                    if (_ !== 0 && !nick && !(splitText[_].startsWith('*') && splitText[_].endsWith('*'))) {
                        break
                    }

                    finalText += (_ === 0 ? '' : "\n") + splitText[_];

                    if (state.precisionMode || state.multiplayer){
                        break
                    }
                }
                return {text: finalText}
            }
        }
    }

    return {text}
}
// Don't modify this part
modifier(text)