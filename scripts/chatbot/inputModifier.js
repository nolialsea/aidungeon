const invalidCommandMessage = 'Error. Command invalid. Try "/setnameplayer YOUR_NAME" or "/setnameai AI_NAME"'
const doString = '\n> You ' // This is what the client prepends to any Do inputs
const sayString = '\n> You say "' // This is what the client prepends to any Say inputs along with the closing quotation mark
const prefixNameCommand = '/setname'
const aiNameCommand = '/setnameai'
const playerNameCommand = '/setnameplayer'
const nameSwitchCommand = '/switchnames'
const toggleChatModeCommand = '/togglechatmode'
const toggleChatModeCommandAlias = '/cm'
const togglePrecisionModeCommand = '/precisionmode'
const togglePrecisionModeCommandAlias = '/pm'
const setPropertyCommand = '/setproperty'
const setPropertyCommandAlias = '/sp'
const setAiCommand = '/setai'
const setMoodCommand = '/mood'
const multiplayerCommand = '/multiplayer'
const multiplayerMoodCommand = '/multimood'
const summaryCommand = '/summary'

const ERROR_SINGLEPLAYER_MISSING_PLAYER_OR_AI_NAME = "ERROR: Set your Player name with /setnameplayer YourName and the AI's name with /setnameai AiName"
const ERROR_SINGLEPLAYER_SAY_MODE = 'ERROR: You are in DO MODE, please switch to SAY MODE or STORY MODE (both are the same anyway)'
const ERROR_MULTIPLAYER_SAY_MODE = "ERROR: Multiplayer gameplay is only intended to use with SAY MODE only"
const ERROR_MULTIPLAYER_THIRD_PERSON_MODE = 'Please enable 3rd Person Only in Edit Adventure to play multiplayer mode'
const ERROR_MULTIPLAYER_MOOD_DISABLED = "ERROR: mood command is only available in single player"
const ERROR_MULTIPLAYER_MSG_TOO_LONG = 'ERROR: Multiplayer messages should be 1000 characters or less'

// This function takes untrimmed text and checks for the input mode
// For use in the switch statement found in the main loop. See shared library for the strings that are prepended to each input type
function modeCheck(text) {
    console.log("Performing mode check...")
    // Mono player SAY
    if (text.startsWith(sayString)) {
        return 'say'
    }
    // Mono player DO
    else if (text.startsWith(doString)) {
        return 'do'
    }
    // Mono player STORY
    else if (!text.startsWith('\n>')) {
        return 'story'
    }
    // Multi player SAY
    else {
        return 'multi'
    }
}

// Sets the name of player or AI
function funcSetName(line) {
    if (line.startsWith(aiNameCommand)) {
        state.aiName = line.slice(aiNameCommand.length + 1)
        state.message = 'AI name set to ' + state.aiName + '.'
        console.log("Setting AI name to " + state.aiName)
    } else if (line.startsWith(playerNameCommand)) {
        state.playerName = line.slice(playerNameCommand.length + 1)
        state.message = 'Player name set to ' + state.playerName + '.'
        console.log("Setting Player name to " + state.playerName)
    } else {
        state.message = invalidCommandMessage
    }
}

function switchNames() {
    const playerName = state.playerName
    state.playerName = state.aiName
    state.aiName = playerName
    state.message = "Player's name set to " + state.playerName + ".\nAI name set to " + state.aiName
    console.log("Switched Player and AI names")
}

function toggleChatMode() {
    state.chatMode = !state.chatMode
    state.message = "ChatMode is now " + (state.chatMode ? 'ENABLED' : 'DISABLED')
    console.log("ChatMode " + (state.chatMode ? 'ENABLED' : 'DISABLED'))
}

function togglePrecisionMode(line, commandAlias = togglePrecisionModeCommand) {
    let command = line.slice(commandAlias.length + 1)

    if (command && command.toLowerCase() === 'off') {
        state.precisionMode = false
    } else if (command && command.toLowerCase() === 'on') {
        state.precisionMode = true
    } else {
        state.precisionMode = !state.precisionMode
    }

    state.message = "Precision Mode is now " + (state.precisionMode ? 'ENABLED' : 'DISABLED')
    console.log("Precision Mode " + (state.precisionMode ? 'ENABLED' : 'DISABLED'))
}

function setMood(line) {
    const rawMood = line.slice(setMoodCommand.length + 1)
    if (rawMood) {
        if (state.multiplayer) {
            state.message = ERROR_MULTIPLAYER_MOOD_DISABLED
            return
        }
        state.aiMood = rawMood
    } else {
        state.aiMood = null;
    }
    state.message = state.aiName + `'s mood set to "` + state.aiMood + `"`
    console.log("Single player AI mood set to " + state.aiMood)
}

function setProperty(line, commandUsed) {
    const temp = line.slice(commandUsed.length + 1).split(" ")
    const property = temp.shift()
    const value = temp.join(" ")
    if (property) {
        const worldInfoEntry = findWorldInfoByKeys(state.multiplayer ? state.talkingPlayer : state.aiName)
        let ai = null
        if (worldInfoEntry) {
            ai = getJSONFromWorldEntry(worldInfoEntry)
        } else {
            ai = {
                json: {
                    name: state.multiplayer ? state.talkingPlayer : state.aiName
                }
            }
        }

        if (value) {
            ai.json[property] = value
        } else {
            ai.json[property] = undefined
        }

        const newWorldEntry = `[${JSON.stringify(ai.json)}]`
        if (worldInfoEntry) {
            updateWorldEntry(worldInfoEntry.index, worldInfoEntry.keys, newWorldEntry)
        } else {
            addWorldEntry(state.multiplayer ? state.talkingPlayer : state.aiName, newWorldEntry)
        }

        const message = (state.multiplayer ? state.talkingPlayer : state.aiName) + `'s property "${property}" set to "` + (value ? value : undefined) + `"`
        state.message = {
            text: message + (state.multiplayer ? ' (only visible by you)' : ''),
            visibleTo: [state.talkingPlayer]
        }
    } else {
        state.message = `ERROR: no property was entered. Use like this: "/setproperty NAME VALUE" (VALUE is optional, and will remove the property if omitted)`
    }
}

function setAi(line, prefix = "Your android: ") {
    const splitLine = line.slice(setAiCommand.length + 1).split(" ")
    let aiName = null
    let jsonStr = null

    if (!state.multiplayer) {
        aiName = splitLine.shift()
    } else {
        prefix = ""
        aiName = state.talkingPlayer
    }
    jsonStr = splitLine.join(" ")

    if (aiName) {
        const worldInfoEntry = findWorldInfoByKeys(aiName)

        if (!jsonStr) {
            worldInfo.forEach((entry, index) => {
                if (entry.keys.toLowerCase() === aiName.toLowerCase()) {
                    removeWorldEntry(index)
                    state.message = `World entry "${aiName}" deleted`
                }
            })
        } else {
            try {
                // Parse/stringify inside try/catch to validate JSON
                const json = JSON.parse(jsonStr)
                const worldEntry = `[${prefix}${JSON.stringify(json)}]`

                if (worldInfoEntry) {
                    updateWorldEntry(worldInfoEntry.index, worldInfoEntry.keys, worldEntry)
                } else {
                    addWorldEntry(aiName, worldEntry)
                }

                const message = `AI "${aiName}" set to ${worldEntry}`
                state.message = {
                    text: message + (state.multiplayer ? ' (only visible by you)' : ''),
                    visibleTo: [state.talkingPlayer]
                }
            } catch (e) {
                state.message = "Error: Invalid JSON (" + e + ")"
            }
        }
    }
}

function toggleMultiplayer(line) {
    let command = line.slice(multiplayerCommand.length + 1)

    if (command && command.toLowerCase() === 'off') {
        state.multiplayer = false
    } else if (command && command.toLowerCase() === 'on') {
        state.multiplayer = true
    } else {
        state.multiplayer = !state.multiplayer
    }

    state.message = "Multiplayer is now " + (state.multiplayer ? 'ENABLED' : 'DISABLED')
    console.log("Multiplayer " + (state.multiplayer ? 'ENABLED' : 'DISABLED'))
}

function toggleMultiplayerMood(line) {
    let command = line.slice(multiplayerMoodCommand.length + 1)

    if (command && command.toLowerCase() === 'off') {
        state.multiplayerMood = false
    } else if (command && command.toLowerCase() === 'on') {
        state.multiplayerMood = true
    } else {
        state.multiplayerMood = !state.multiplayerMood
    }

    state.message = "Multiplayer Mood is now " + (state.multiplayerMood ? 'ENABLED' : 'DISABLED')
    console.log("Multiplayer Mood " + (state.multiplayerMood ? 'ENABLED' : 'DISABLED'))
}

// Trims \n> You say "inputtext"\n to just "inputtext"
function trimSayText(text) {
    return text.slice(sayString.length, text.length - 2)
}

// Parses commands and removes them from text
// TODO: refacto
function parseCommands(text) {
    const lines = text.split('\n')
    const startsWithNewLine = text.startsWith('\n')
    const endsWithNewLine = text.endsWith('\n')
    let parsedText = []
    let shouldUnstop = false

    for (let line of lines) {
        if (line.toLowerCase().startsWith(prefixNameCommand)) {
            funcSetName(line)
            state.shouldStop = true
        } else if (line.toLowerCase().startsWith(nameSwitchCommand)) {
            switchNames(line)
            state.shouldStop = true
        } else if (line.toLowerCase().startsWith(toggleChatModeCommand)) {
            toggleChatMode()
            state.shouldStop = true
        } else if (line.toLowerCase().startsWith(toggleChatModeCommandAlias)) {
            toggleChatMode()
            state.shouldStop = true
        } else if (line.toLowerCase().startsWith(togglePrecisionModeCommand)) {
            togglePrecisionMode(line)
            state.shouldStop = true
        } else if (line.toLowerCase().startsWith(togglePrecisionModeCommandAlias)) {
            togglePrecisionMode(line, togglePrecisionModeCommandAlias)
            state.shouldStop = true
        } else if (line.toLowerCase().startsWith(setMoodCommand)) {
            setMood(line)
            state.shouldStop = true
        } else if (line.toLowerCase().startsWith(setPropertyCommand)) {
            setProperty(line, setPropertyCommand)
            state.shouldStop = true
        } else if (line.toLowerCase().startsWith(setPropertyCommandAlias)) {
            setProperty(line, setPropertyCommandAlias)
            state.shouldStop = true
        } else if (line.toLowerCase().startsWith(setAiCommand)) {
            setAi(line)
            state.shouldStop = true
        } else if (line.toLowerCase().startsWith(multiplayerCommand)) {
            toggleMultiplayer(line)
            state.shouldStop = true
        } else if (line.toLowerCase().startsWith(multiplayerMoodCommand)) {
            toggleMultiplayerMood(line)
            state.shouldStop = true
        } else {
            parsedText.push(line)
            shouldUnstop = true
        }
    }

    if (shouldUnstop) {
        state.shouldStop = false
    }

    let returnText = parsedText.join('\n')
    if (startsWithNewLine) {
        returnText = '\n' + returnText
    }
    if (endsWithNewLine) {
        returnText += '\n'
    }

    return returnText
}

// Trims things added by DO and STORY mode like quotes and new lines
function trimText(text, mode) {
    if (mode === 'say') {
        return trimSayText(text)
    } else if (mode === 'story') {
        // Trims \n at the start of text
        return text.slice(state.firstTurn ? 0 : 1)
    } else if (mode === 'multi') {
        const matchSayMode = text.match(/\n> ([a-zA-Z0-9 '_-]*) says "/)

        // A multiplayer player says something
        if (matchSayMode && matchSayMode[1]) {
            state.talkingPlayer = matchSayMode[1]
            console.log("Multiplayer player name: " + state.talkingPlayer)
            return text.slice(matchSayMode[0].length, text.length - 2)
        } else {
            state.message = ERROR_MULTIPLAYER_SAY_MODE
            state.shouldStop = true
            return ''
        }
    }
}

const modifier = (text) => {
    initTurn()
    let modifiedText = text

    // Get mode and throw error if Player is in DO mode
    const mode = modeCheck(text)
    if (mode === 'do') {
        state.message = ERROR_SINGLEPLAYER_SAY_MODE
        return {text: '', stop: true}
    } else if (state.multiplayer && mode !== 'multi') {
        if (state.multiplayer && (!info.characters || (info.characters.length === 1 && !info.characters[0].name))) {
            state.message = ERROR_MULTIPLAYER_THIRD_PERSON_MODE
        } else {
            state.message = ERROR_MULTIPLAYER_SAY_MODE
        }
        state.shouldStop = true
        return {text: '', stop: true}
    }

    // Trims things added by DO and STORY mode like quotes and new lines
    modifiedText = trimText(modifiedText, mode)

    if (state.shouldStop) {
        return {
            text: '',
            stop: true
        }
    }

    // Parses commands and remove their lines from input text
    modifiedText = parseCommands(modifiedText)

    if (!state.multiplayer) {
        console.log("Single player message processing")
        if (state.chatMode) {
            console.log("Chat mode modifiers")
            // This is the default chatbot format for Say mode. Triggers if it doesn't detect a command
            if (!modifiedText.startsWith(',') && !modifiedText.startsWith(';') && !state.firstTurn) {
                modifiedText = '\n' + state.playerName + ': ' + modifiedText + '\n' + state.aiName + ':'
            }
            // If comma is detected at the start of say input, starts the next line with aiName: and confines output there
            else if (modifiedText.startsWith(',')) {
                modifiedText = '\n' + state.aiName + ':' + modifiedText.slice(1) //Slices the comma off as the first character of the string, leaving only the text that comes after
            }
            // If semi-colon is detected at the start of say input, starts the next line with playerName: and confines output there
            else if (modifiedText.startsWith(';')) {
                modifiedText = '\n' + state.playerName + ':' + modifiedText.slice(1)
            }
        }else{
            console.log("Not in chat mode, using text as it is")
        }
    } else if (!state.firstTurn) {
        console.log("Multiplayer message processing")
        if (modifiedText.length > 1000) {
            state.message = ERROR_MULTIPLAYER_MSG_TOO_LONG
            console.log("Multiplayer message too long")
            return {text: '', stop: true}
        } else if (state.talkingPlayer) {
            state.playerMessage = modifiedText + ""
            if (state.multiplayerMood) {
                modifiedText = '\n' + state.talkingPlayer + ':'
                console.log("Multiplayer player input for mood: " + state.playerMessage)
            } else {
                console.log("Multiplayer player input: " + state.playerMessage)
            }
        } else {
            console.log("There was no talking player, nothing happens")
        }
    } else {
        console.log("Multiplayer first turn (stopped by default)")
    }

    // Displays action count at the upper left of the screen
    state.displayStats = [{
        key: 'Action count',
        value: info.actionCount
    }]

    if (!state.multiplayer) {
        state.displayStats.push({
            key: '\nChat Mode',
            value: state.chatMode
        })
        state.displayStats.push({
            key: '\nPrecision Mode',
            value: state.precisionMode
        })

        if (state.aiMood) {
            state.displayStats.push({
                key: `\n${state.aiName}'s mood`,
                value: state.aiMood
            })
        }
    } else {
        state.displayStats.push({
            key: '\nMultiplayer',
            value: state.multiplayer
        })
        state.displayStats.push({
            key: '\nMultiplayer Mood',
            value: state.multiplayerMood
        })
    }

    // Error if the player didn't enter a name for both the player and the AI
    if (!state.multiplayer && (!state.playerName || !state.aiName)) {
        state.message = ERROR_SINGLEPLAYER_MISSING_PLAYER_OR_AI_NAME
    }

    if (state.multiplayer) {
        if (state.multiplayerMessageIsProcessing) {
            console.log("Multiplayer was already processing")
            const message =
                "ERROR: Another message is still processing..."
                + "Below is your message so you can copy and paste it (visible only by you)\n"
                + state.playerMessage
            state.message = {text: message, visibleTo: [state.talkingPlayer]}
            state.shouldStop = true
        } else {
            console.log("Multiplayer is not processing yet")
            if (state.shouldStop) {
                state.multiplayerMessageIsProcessing = false
                console.log("Multiplayer processing stopped because of shouldStop")
            } else {
                state.multiplayerMessageIsProcessing = true
                // Make the AI talk like in single player
                if (state.playerMessage) {
                    if (state.playerMessage.startsWith(',')) {
                        console.log("Command ',' detected")
                        if (state.playerMessage.length === 1) {
                            modifiedText = '\n' + state.aiName + ':'
                        } else {
                            modifiedText = '\n' + state.talkingPlayer + ':' + state.playerMessage.substring(1) + '\n' + state.aiName + ':'
                        }
                    } else {
                        modifiedText = '\n' + state.talkingPlayer + ': ' + state.playerMessage
                        state.shouldStopInContextMod = true
                        state.multiplayerMessageIsProcessing = false
                        console.log("Multiplayer processing stopped because of normal message")
                    }
                }
            }
        }
    }

    console.log(state.firstTurn, state.shouldStop, modifiedText)
    return {
        text: modifiedText,
        stop: state.firstTurn ? false : state.shouldStop
    }
}

modifier(text)