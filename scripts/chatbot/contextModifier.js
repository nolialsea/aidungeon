function makeSpaceInContext(contextStr, neededSpace) {
    const lines = contextStr.split('\n')
    const linesFinal = []
    let freedSpace = 0

    for (let line of lines) {
        if (!(line.startsWith('[') && line.endsWith(']'))) {
            if (freedSpace < neededSpace) {
                freedSpace += line.length
            } else {
                linesFinal.push(line)
            }
        } else {
            linesFinal.push(line)
        }
    }

    return linesFinal.join('\n')
}

const modifier = (text) => {

    if (state.shouldStopInContextMod) {
        return {text, stop: true}
    }

    let modifiedText = text + ""

    console.log("Context modifier")

    if (state.aiMood && !state.multiplayer) {
        const moodText = `[${state.aiName}'s mood: ${state.aiMood}.]`
        modifiedText = makeSpaceInContext(modifiedText, moodText.length + 1)
        const splitText = modifiedText.split('\n')

        splitText.splice(splitText.length - 2, 0, moodText)
        modifiedText = splitText.join('\n')
    } else if (state.multiplayer) {
        console.log("Context modifier multiplayer")
        if (state.multiplayer && (!state.playerMessage || !state.talkingPlayer)) {
            if (!state.firstTurn) {
                state.message = 'ERROR: Either player couldn\'t be determined or message is empty'
                state.multiplayerMessageIsProcessing = false
                return {text: '', stop: true}
            }
        } else if (state.multiplayerMood && state.playerMessage && !state.playerMessage.startsWith(',')) {
            console.log("Multiplayer mood injected")
            const moodText = `[${state.talkingPlayer}'s mood: ${state.playerMessage}.]`
            modifiedText = makeSpaceInContext(modifiedText, moodText.length + 1)
            const splitText = modifiedText.split('\n')

            splitText.splice(splitText.length - 1, 0, moodText)
            modifiedText = splitText.join('\n')

            state.message = {text: `Injected: ${moodText} (only visible by you)`, visibleTo: [state.talkingPlayer]}
        }
    }

    if (state.firstTurn) {
        state.firstTurn = false
        state.multiplayerMessageIsProcessing = false
        return {text, stop: true}
    }


    return {text: modifiedText}
}

// Don't modify this part
modifier(text)