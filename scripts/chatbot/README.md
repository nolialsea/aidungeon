# Chatbot Script
This script is intended to simulate a conversation between two or more parties

## Features

- 1v1 conversation (from original script in credits)
- set/change your name
- set/change AI name
- Multiple consecutive messages from the chatbot
- Disabled DO mode completely (was causing formatting problems)
- Switch between Player/AI, take control of the AI with `/switchnames`!
- Parse multiple commands at once
- Narration lines! Keeps every line beginning and ending by `*`
- Enable/disable Chat Mode (cuts your own messages from the answer)
- Enable/disable Precision Mode (cuts at the end of the line)
- [NEW] Set the AI's actual mood! (injects the mood before your last message)
- [NEW] add/edit/remove properties of the AI
- [NEW] Multiplayer integration
- [NEW] Set AI name at startup (placeholder search/replace in first prompt)
- [NEW] World Info commands (MP, non-host players cannot edit WI, this is a workaround)
- [TODO] World Info generation based on initial questions

## Single Player Commands

- `,` will trigger the AI to send a message
- `, TEXT` will trigger the AI to send a message starting with `TEXT`
- `;` will trigger the AI to send a message on your name
- `; TEXT` will trigger the AI to send a message on your name, starting with `TEXT`
- `/setnameplayer PLAYER_NAME` sets player name to `PLAYER_NAME`
- `/setnameai AI_NAME` sets ai name to `AI_NAME`
- `/switchnames` switches player and ai name
- `/togglechatmode` enables/disables chat mode (truncating generated player's messages)
- `/cm` alias for `/togglechatmode`
- `/precisionmode` toggles precision mode (keeps only the next message)
- `/precisionmode on` enables precision mode
- `/precisionmode off` disables precision mode
- `/pm` alias for `/precisionmode`
- `/mood MOOD` sets the mood of the AI, used like Author's Note (does not use AN though)
- `/setproperty PROPERTY VALUE_WITH_SPACES_IF_YOU_WANT_I_DON_T_GIVE_A_FUCK` sets a property to the AI
- `/setproperty PROPERTY` erases the property. Oh, you just forgot to put a value? Fuck you.
- `/sp` alias for `/setproperty`
- `/setai AI_NAME AI_JSON` updates the WI entry for an AI (you can add as many AIs as you want)
- `/setai AI_NAME` erases the WI. Oh, you just forgot to paste the JSON? Well... Yeah, I shouldn't code when drunk


## Multiplayer Commands
This is a small multiplayer mini game concept  
You join the same game with multiple persons (3rd person must be enabled) and just... Chat  
The twist is, you can't talk directly, all you can do is give a "mood" for the AI that will write your message!  

- `/multiplayer` toggles multiplayer mode
- `/setai AI_JSON` to write the WI corresponding to your nick and give a little personality to your own AI!
- `/setproperty PROPERTY VALUE` to write indivudual properties inside your AI's WI entry
- `/sp` is an alias for `/setproperty`
- `,` will trigger the AI (Alice by default) to talk
 

# Credits

Even though this script is now heavily modified, credits goes to Matty1112, the original creator of this script  

Original script: https://github.com/Matty1112/AID_chatbot_script_v2