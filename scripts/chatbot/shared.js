if (!state.setup) {
    state.setup = true // Ensure this is only set once and never wiped.
    state.playerName = 'You' // Makes a state for the Player's name. To be set later. Defaults to 'You' if not set by user.
    state.aiName = 'Alice' // Makes a state for the AI's name. To be set later. Defaults to 'AI' if not set by user.
    state.chatMode = true // State to toggle the one-line behavior in output modifier.
    state.firstTurn = true // Used to know if we are in the very first turn or not
    state.messageShouldStay = false // Used to keep the previous message or emptying it each turn
    state.precisionMode = true // Used to truncate anything after the first new line
    state.aiMood = null // Injects the mood of the AI like Author's Note does (does not use AN)
    state.aiPersonaWorldInfoId = null // too drunk

    state.multiplayer = false // Multiplayer mode
    state.multiplayerMood = false // Multiplayer mini game
    state.talkingPlayer = null // Used for multiplayer to hold the name of the player who talks
    state.playerMessage = null // Used for multiplayer to hold the message of the player who talks (used in context mod)

    state.multiplayerMessageIsProcessing = false // Alleviate spamming problems during multiplayer, hopefully
}

function findWorldInfoByKeys(keys) {
    for (let i=0; i<worldInfo.length; i++) {
        worldInfoEntry = worldInfo[i]
        if (worldInfoEntry.keys.toLowerCase() === keys.toLowerCase()) {
            worldInfoEntry.index = i
            return worldInfoEntry
        }
    }
}

function getJSONFromWorldEntry(entry) {
    if (entry) {
        let aiPersonaJsonTemp = entry.entry.substring(1)
        const aiPersonaJson = aiPersonaJsonTemp.substring(0, aiPersonaJsonTemp.length - 1)
        return {json: JSON.parse(aiPersonaJson), worldInfoEntry: entry}
    }
}

function initTurn() {
    state.shouldStop = false // Used by various commands to stop the request
    state.shouldStopInContextMod = false

    if (!state.multiplayer) {
        state.talkingPlayer = null
        state.playerMessage = null
    }

    // Clears the info message at the bottom of the screen
    if (!state.messageShouldStay) {
        delete state.message
    }
}